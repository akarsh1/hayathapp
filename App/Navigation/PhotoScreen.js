import React, {Component} from "react";
import {Platform, Image, StyleSheet, Dimensions} from "react-native";
import {Tools, Images, Constants} from '@common';
import {Photos} from '@container';
import Icons from '@navigation/Icons';
import {TabBarIcon} from "@components";

const {width} = Dimensions.get("window"), vw = width / 100;

export default class PhotoScreen extends Component {
  
  static navigationOptions = {
    headerTitle: 'Live TV',
    headerStyle: {
        backgroundColor: '#2C358E'
    },  
    headerTitleStyle: {
      textAlign: "center",
        color: '#fff'
    }
  } 
    

 render() { 
    return (
      <Photos
      />
    )
  }
}

