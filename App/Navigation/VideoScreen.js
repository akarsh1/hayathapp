/** @format */

import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Videos } from '@container'
import { withNavigationFocus } from "react-navigation";
class VideoScreen extends Component {
  
  
static navigationOptions = {
  headerTitle: 'Live TV',
  headerStyle: {
      backgroundColor: 'red'
  },  
  headerTitleStyle: {
      color: '#fff'
  }
} 
  static propTypes = {
    navigation: PropTypes.object.isRequired,
  }

  render() {
    const { navigate, onBack } = this.props.navigation
    return (
      <Videos
        onBack={() => onBack()}
        isFocused={this.props.isFocused}
        onViewPost={(item, index, parentPosts) =>
          navigate("postDetail", {
            post: item,
            index,
            parentPosts,
            backToRoute: "video"
          })
        }
      />
    ); 
  }
}
export default withNavigationFocus(VideoScreen);