import React, {Component} from "react";
import {Platform, Image, StyleSheet, Dimensions} from "react-native";
import {Tools, Images, Constants} from '@common';
import {Photos} from '@container';
import Icons from '@navigation/Icons';
import {TabBarIcon} from "@components";

const {width} = Dimensions.get("window"), vw = width / 100;

export default class YoutubeScreen extends Component {
  static navigationOptions = {
    title: 'Photo',
    tabBarLabel: 'Photo',
    tabBarIcon: ({tintColor}) => (
      <TabBarIcon icon={Images.icons.photo} tintColor={tintColor}/>
    ),
    headerLeft: Icons.Home(),
  };
 
  render() {
    const { navigate, onBack } = this.props.navigation
 
    return (
      <Photos
         onViewPost={(id) =>
            navigate("YouTubeVideo", {
            youtubeId: id,
            backToRoute: "photo"
        })
      }
      />
    )
  }
}
