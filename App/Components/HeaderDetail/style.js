/** @format */

import { StyleSheet, Platform, Dimensions } from 'react-native'
import { Constants } from '@common'

export default StyleSheet.create({
  container:{
    height: 30,
    backgroundColor:'#E02726',
    flexDirection: 'row',
    alignItems:'center',
    justifyContent:'space-between',
    paddingHorizontal: 10
  },
  backIcon:{
    width: 16,
    height: 16,
    resizeMode:'contain'
  },
  icon:{
    width: 20,
    height: 20,
    resizeMode:'contain'
  }
})
