/** @format */

import React , { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { View,Dimensions, ActivityIndicator } from 'react-native'
import Color from '@common/Color'
import styles from './styles'
import { Lottie  } from '@expo';
const { width, height } = Dimensions.get('window')
// const Spinkit = ({ css }) => {
  class Spinkit extends PureComponent {
    componentDidMount() {
      this.animation.play();
      // Or set a specific startFrame and endFrame with:
      // this.animation.play(30, 120);
    }
    resetAnimation = () => { 
      this.animation.reset();
      this.animation.play();
    };
    constructor(props) {
      super(props);
  
      
    }
    render() {
   return (
      <View style={[styles.spinner, typeof css !== 'undefined' ? css : null]}>
      {/* <ActivityIndicator color={Color.spin} /> */}
     <Lottie
        ref={animation => {
            this.animation = animation;
          }}
           style={{ 
            width: 100,
            height: 100,
            backgroundColor: '#eee',
          }}
          source={require('./paper.json')}
          // OR find more Lottie files @ https://lottiefiles.com/featured
          // Just click the one you like, place that file in the 'assets' folder to the left, and replace the above 'require' statement
        />
    </View>
    
  )
}
  }
// Spinkit.propTypes = {
//   css: PropTypes.any, 
// }

export default Spinkit
