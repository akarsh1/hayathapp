/** @format */

import React, { PureComponent } from 'react'
import {View, Text, TouchableOpacity, Image, TextInput, KeyboardAvoidingView, Platform} from 'react-native'
import { Constants, Config, Images, Languages } from '@common'
import styles from './style'
import moment from 'moment'
import {ImageCache} from '@components'

class PostHeading extends PureComponent {

  render() {
    let {onChangeText, style, required} = this.props
    return (
      <View style={[styles.container, style, required && styles.required]}>
        <Text style={styles.title}>{Languages.postHeading}</Text>
          <TextInput
            style={styles.input}
            underlineColorAndroid='transparent'
            autoCapitalize='none'
            autoCorrect={false}
            onChangeText={onChangeText} />
      </View>

    )
  }
}

export default PostHeading
