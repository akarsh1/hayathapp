import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  Linking,
  ScrollView,
  Animated,
  Button,
  Image
} from "react-native";
import { Avatar } from "@components";
import _ from "lodash";
import Base from "./Base";
import css from "./style";

import PropTypes from "prop-types";
import { flatten } from "lodash";
import {
  fetchCategories,
  setActiveLayout,
  setActiveCategory
} from "@redux/actions";
import { connect } from "react-redux";
import { Languages } from "@common";

import { CategoryList } from "@components";

class SideMenu extends Base {
  static propTypes = {
    categories: PropTypes.array,
    fetchCategories: PropTypes.func,
    setActiveCategory: PropTypes.func,
    onViewPost: PropTypes.func,
    onViewCategory: PropTypes.func,
    setActiveLayout: PropTypes.func,
    selectedLayout: PropTypes.bool
  };

  componentDidMount() {
    this.props.fetchCategories();
  }

  showCategory = category => {
    const { setActiveCategory, onViewCategory } = this.props;
    setActiveCategory(category.id);

    onViewCategory({ config: { name: category.name, category: category.id } });
  };

  render() {
    const { rowStyle, textColor } = this.props;
    const { userData } = this.state;
    const hasData = !_.isEmpty(userData);
    const { categories, onViewPost, selectedLayout } = this.props;

    // if (!selectedLayout) {
    //return <CategoryList showBanner onViewPost={onViewPost} />
    // }

    return (
      <ScrollView style={{   }}>

<Animated.View style={{
    width: 200,
    height: 200, 
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor:'red',
    borderBottomRightRadius: 15, 
    borderBottomLeftRadius: 15,
  }} >
   <Image
            source={require("@images/logo1.png")}
            style={{ height: 150, width: 150 }}
          />
  </Animated.View>
      {/* <Animated.View >
        <Animated.View style={{
    width: 150,
    height: 75,
    backgroundColor: 'red',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    borderBottomRightRadius: 15,
    borderBottomLeftRadius: 15,
  }} />
        <Animated.View style={{
    width: 73,
    height: 70,
    backgroundColor: 'red', 
    position: 'absolute',
    top: -26,
    left: 39,
    borderRadius: 35,
    transform: [
      {scaleX: 2},
      {scaleY: .5}
    ]
  }} />
        <Animated.View style={{
    width: 73,
    height: 70,
    backgroundColor: 'red',
    position: 'absolute',
    bottom: -26,
    left: 39,
    borderRadius: 35,
    transform: [
      {scaleX: 2},
      {scaleY: .5}
    ]
  }} />
        <Animated.View style={{
    width: 20,
    height: 38,
    backgroundColor: 'red',
    position: 'absolute',
    left: -7,
    top: 18,
    borderRadius: 35,
    transform: [
      {scaleX: .5},
      {scaleY: 2},
    ]
  }} />
        <Animated.View style={{
    width: 20,
    height: 38,
    backgroundColor: 'red',
    position: 'absolute',
    right: -7,
    top: 18,
    borderRadius: 35,
    transform: [
      {scaleX: .5},
      {scaleY: 2},
    ]
  }} />

      </Animated.View>    */}






        <View
          style={{
            paddingLeft: 55,
            marginTop: 10,
            borderBottomWidth: 1,
            borderBottomColor: "red",
            borderBottomEndRadius: 50,
            borderBottomLeftRadius: 50,
            borderBottomRightRadius: 50,
          }}
        >

        </View>

        <View
          style={[
            css.sideMenu,
            this.props.menuBody,
            hasData && { paddingTop: 100 }
          ]}
        >
          {categories.map((category, index) => {
            return (
              <TouchableOpacity
                style={[css.menuRow, rowStyle]}
                key={`category${index}`}
                underlayColor="#2D2D30"
                onPress={() => this.showCategory(category)}
              >
                <Text style={[css.menuLink, textColor]}>{category.name}</Text>
              </TouchableOpacity>
            );
          })}

          {hasData && (
            <TouchableOpacity
              style={[css.menuRowLogout, rowStyle]}
              underlayColor="#2D2D30"
              onPress={this.logout}
            >
              <Text style={[css.logoutLink, textColor]}>
                {Languages.logout}
              </Text>
            </TouchableOpacity>
          )}
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              top: 0,
              alignSelf: "center",
              paddingTop: 40
            }}
          >
            <Text
              style={{
                color: "blue",
                justifyContent: "center",
                alignItems: "center",
                top: 0,
                alignSelf: "center"
              }}
              onPress={() => Linking.openURL("http://thewebpeople.in")}
            >
              Developed by The Web People.
            </Text>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => {
  const selectedCategory = state.categories.selectedCategory;
  const categories = flatten(state.categories.list);
  const selectedLayout = state.categories.selectedLayout;
  return { categories, selectedCategory, selectedLayout };
};
export default connect(
  mapStateToProps,
  {
    fetchCategories,
    setActiveLayout,
    setActiveCategory
  }
)(SideMenu);
