import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { DrawerLayoutAndroid } from 'react-native'
import { Events } from '@common'
import { Modal } from '@components'
import SideMenu from './SideMenu'
import { Style } from '@common'
import Icons from '@navigation/Icons'
import User from '@services/User'

export default class MenuAndroid extends PureComponent {

  constructor(props){
    super(props)
    this.state = { userData: '' }
    this.fetchDataUser()
    Events.onSideMenuRefresh(this.fetchDataUser)
  }

  static propTypes = {
    goToScreen: PropTypes.func,
    routes: PropTypes.any,
  }

  static navigationOptions = {
    headerLeft: Icons.Home(),
    header: null,
    headerStyle: Style.toolbar,
  }

  componentDidMount() {
    this.sideMenuClick = Events.onOpenLeftMenu(this.openSideMenu.bind(this))
    this.sideMenuClose = Events.onCloseLeftMenu(this.closeSideMenu.bind(this))
  }

  componentWillUnmount() {
    // Remove drawer event
    this.sideMenuClick.remove()
    this.sideMenuClose.remove()
  }

  closeSideMenu = () => {
    if (typeof this.drawer !== 'undefined') {
      this.drawer.closeDrawer()
    }
  }

  goToScreen = (routeName, params = {}) => {
    this.props.goToScreen(routeName, params, false)
    Events.closeLeftMenu()
  }
  fetchDataUser = () => {
    const self = this
    User.getUser().then((data) => {
      self.setState({
        userData: data,
      })
    })
  }

  openSideMenu = () => {
    typeof this.drawer !== 'undefined' && this.drawer.openDrawer()
  }

  render() {
    // const { navigate } = this.props.navigation
    return (
      <DrawerLayoutAndroid
        drawerWidth={300}
        ref={(_drawer) => (this.drawer = _drawer)}
        drawerPosition={DrawerLayoutAndroid.positions.Left}
        renderNavigationView={() => (
          <SideMenu 
            onViewCategory={(config) => this.goToScreen('PostListScreen',config)}
            onViewPost={(item, index, parentPosts) =>
              this.goToScreen('postDetail', { post: item, index, parentPosts, backToRoute: 'category' })
        }
        
          />
             
        )}
      >  
      
        {this.props.routes}
        <Modal.Tag />
        <Modal.Layout />
        <Modal.Category />
        <Modal.User />
      </DrawerLayoutAndroid>
    )
  }
}
