import React, {Component} from 'react';
import {View, Text, ScrollView, TouchableHighlight, ImageBackground, FlatList} from 'react-native';

import Youtube from 'react-native-youtube';

export default class VideoPlayer extends Component{
  
    constructor(props){
        super(props);
        this.state = ({
            theme : 'white',
            maxResult : 50
        })
    }
    
    componentWillMount(){
      let api = this.props.navigation.state.params.apiKey;
        let id =this.props.navigation.state.params.videoId;
        let data = this.props.navigation.state.params.data;
        let theme = this.props.navigation.state.params.theme;
        let token = this.props.navigation.state.params.token;
        let channelId = this.props.navigation.state.params.channelId;
        this.setState({videoId : id, theme : theme, data : data, token : token, channelId : channelId, apiKey : api});
    }
    addVideos = ()=>{
      fetch(`https://www.googleapis.com/youtube/v3/search/?key=${this.state.apiKey}&channelId=${this.state.channelId}&part=snippet,id&order=date&maxResults=${this.state.maxResult}&pageToken=${this.state.token}`).then(res=> res.json()).then(res=>{
      let dat = this.state.data;
      let data = dat.concat(res.items);
      this.setState({data : data, isLoading : false, token : res["nextPageToken"]})
      })
    }
    render(){
        return(
            <View style = {{backgroundColor : this.state.theme=='white'?white.mainBackgroundColor : black.mainBackgroundColor}}>
            
            <View>
                <Youtube ref = {(youtube)=> this.youTub = youtube}
                apiKey = {this.state.apiKey}
                videoId={this.state.videoId}   // The YouTube video ID
                play={true}             // control playback of video with true/false
                fullscreen={true}       // control whether the video should play in fullscreen or inline
                loop={false}             // control whether the video should loop when ended
 
                onReady={e => this.setState({ isReady: true })}
                onChangeState={e => this.setState({ status: e.state })}
                onChangeQuality={e => this.setState({ quality: e.quality })}
                onError={e => this.setState({ error: e.error })}
                style = {{alignSelf : 'stretch', height : 250}}
                />
            </View>
            <FlatList
            data = {this.state.data}
            keyExtractor = {(item,index)=>index.toString()}
            renderItem = {this.renderItemd}
            />
            
           
            </View>
        )
    }
    renderItems1 = ()=>{
        let data = this.state.data;
        return data.map((item,index)=>{
          return(
            <TouchableHighlight underlayColor = {"rgba(180,180,180,0.5)"} key = {index} style = {{marginVertical : 5,marginHorizontal : 4, borderBottomColor : 'rgba(180,180,180,0.5)', borderWidth : 0, padding : 5,elevation :1, backgroundColor : this.state.theme=='white'?white.itemBackground : black.itemBackground}} onPress = {()=>{this.setState({videoId : item.id.videoId})}} >
              <View style = {{flexDirection : 'row'}}>
                <View style = {{flex : 0.4}}>
                  <ImageBackground source = {{uri : item.snippet.thumbnails.medium.url}} style = {{height : 80}}></ImageBackground>
                </View>
                <View style = {{flex : 0.6, padding : 5}}>
                  <Text style = {{color : this.state.theme=='white'? white.textColor : black.textColor}}>{item.snippet.title}</Text>
                </View>
              </View>
            </TouchableHighlight>
          )
        })
      }
      renderItemd = ({item,index})=>{
        return(
          <TouchableHighlight underlayColor = {"rgba(180,180,180,0.5)"} key = {index} style = {{marginVertical : 5,marginHorizontal : 4, borderBottomColor : 'rgba(180,180,180,0.5)', borderWidth : 0, padding : 5,elevation :1, backgroundColor : this.state.theme=='white'?white.itemBackground : black.itemBackground}} onPress = {()=>{this.setState({videoId : item.id.videoId})}} >
              <View style = {{flexDirection : 'row'}}>
                <View style = {{flex : 0.4}}>
                  <ImageBackground source = {{uri : item.snippet.thumbnails.medium.url}} style = {{height : 80}}></ImageBackground>
              </View>
              <View style = {{flex : 0.6, padding : 5}}>
                  <Text style = {{color : this.state.theme=='white'? white.textColor : black.textColor}}>{item.snippet.title}</Text>
              </View>
              </View>
            </TouchableHighlight>
        )
      }
}
const white = {
    mainBackgroundColor : 'rgba(180,180,180,0.2)',
    textColor : 'black',
    itemBackground : 'white'
  }
  const black = {
    mainBackgroundColor : 'rgb(10,10,10)',
    textColor : 'white',
    itemBackground : 'rgba(20,20,20,0.9)'
  }
  