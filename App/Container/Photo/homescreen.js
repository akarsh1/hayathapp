import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, FlatList, ScrollView, TouchableHighlight, ImageBackground, Image, Modal, ActivityIndicator} from 'react-native';
export default class HomeScreen extends React.Component {
    constructor(props){
      super(props);
      this.state = ({
        name : 'saurabh',
        data : 'name',isLoading : true, token : 'saurbah',
        daut : 'fhhf',
        theme : 'white',
        apiKey : "AIzaSyDex0XjrRHCW-tI-C9oeISVvd_6mjJ9qfE",// put your youtube api key here
        channelId : 'UCupvZG-5ko_eiXAupbDfxWw',
        maxResult : 50,
        items : 'jkfdjkdf',
        isVisible : false
      })
    }
    componentDidMount(){
      //this.dat();
      //this.themeSetter();
      fetch(`https://www.googleapis.com/youtube/v3/search/?key=${this.state.apiKey}&channelId=${this.state.channelId}&part=snippet,id&order=date&maxResults=${this.state.maxResult}`).then(res=> res.json()).then(res=>{
        this.setState({data : res["items"], isLoading : false, token : res["nextPageToken"]})
      })
    }
    themeSetter = async ()=>{
      if(await AsyncStorage.getItem('theme')==undefined){
        let them = 'black';
        await AsyncStorage.setItem('theme',JSON.stringify(them));
        this.setState({theme : 'black'})
      }
      else{
        let them = await AsyncStorage.getItem('theme');
        this.setState({theme : JSON.parse(them)});
      }
    }
    changeTheme = async (theme)=>{
      await AsyncStorage.setItem('theme',JSON.stringify(theme));
      let them = await AsyncStorage.getItem('theme');
      this.setState({theme : JSON.parse(them)});
    }
    ren = ()=>{
      let data = this.state.data;
      let text = [];
      for(key in data){
        text.push(key);
      }
      return text.join(" ");
    }
  
    // loading more videos
    addVideos = ()=>{
      let token = this.state.token;
      fetch(`https://www.googleapis.com/youtube/v3/search/?key=${this.state.apiKey}&channelId=${this.state.channelId}&part=snippet,id&order=date&maxResults=${this.state.maxResult}&pageToken=${token}`).then(res=> res.json()).then(res=>{
      let dat = this.state.data;
      let data = dat.concat(res.items);
      this.setState({data : data, isLoading : false, token : res["nextPageToken"]})
      })
    }
    setChannel = (id)=>{
      this.setState({channelId : id, isLoading:true});
      fetch(`https://www.googleapis.com/youtube/v3/search/?key=${this.state.apiKey}&channelId=${id}&part=snippet,id&order=date&maxResults=${this.state.maxResult}`).then(res=> res.json()).then(res=>{
        this.setState({data : res["items"], isLoading : false, token : res["nextPageToken"]})
      })
    }
    dat = ()=>{
      firebase.database().ref("users/").once('value',(data)=>{
        let dat = data.toJSON();
        let dau = dat[0]["name"];
        this.setState({items : dau});
      })
    }
  
    render() {
      if(!this.state.isLoading){
      return(
        <View style = {this.state.theme=="white" ? {backgroundColor : white.mainBackgroundColor} : {backgroundColor : black.mainBackgroundColor}}>
        
        <Text onPress = {()=>{this.setState({isVisible : true})}} style = {{color : 'white'}}>change theme to white</Text>
        <Text onPress = {()=>{this.changeTheme('black')}}>{this.state.items}</Text>
        <Text style = {{padding : 5, backgroundColor : 'cyan'}} onPress = {()=>{this.props.navigation.navigate('ChannelChange',{getChannel : (data)=>{this.setChannel(data)}, apiKey : this.state.apiKey})}}>
        Change Channel{this.state.items}</Text>
        
        <ScrollView>
        {this.renderItems1()}
        <Text onPress = {()=>{this.addVideos()}} style = {{backgroundColor : 'cyan', fontSize : 25, textAlign : 'center', marginBottom : 80}}>
        Load More 
         </Text>
        </ScrollView>
  
        <Modal visible ={this.state.isVisible} transparent = {true}>
        <View style = {{flex : 1, alignItems : 'stretch', justifyContent : 'center', alignSelf :'stretch'}}>
        <View style = {{backgroundColor : 'white'}}>
            <Text style = {{fontSize : 25, backgroundColor : 'rgba(200,200,200,0.8)', margin : 5}} onPress = {()=>{this.setState({isVisible : false})}}>Choose Your Theme</Text>
            <View>
            <TouchableHighlight underlayColor = {'gray'} onPress = {()=>{this.changeTheme('white'), this.setState({isVisible : false})}} style = {{margin : 5}}><Text style = {{backgroundColor : 'white', padding : 5, borderWidth :0.5, borderColor : 'rgba(180,180,180,0.5)', alignSelf : 'stretch', fontSize : 24, textAlign : 'center'}}>Light</Text></TouchableHighlight>
            <TouchableHighlight underlayColor = {'gray'} onPress = {()=>{this.changeTheme('dark'), this.setState({isVisible : false})}} style = {{margin : 5}}><Text style = {{backgroundColor : 'white', padding : 5, borderWidth :0.5, borderColor : 'rgba(180,180,180,0.5)', alignSelf : 'stretch', fontSize : 24, textAlign : 'center'}}>Dark</Text></TouchableHighlight>
            </View>
        </View>
        </View>
        </Modal>
  
        </View>
      )
      }
      else
      return(
        <View style = {{justifyContent : 'center', alignItems : 'center', flex : 1}}><ActivityIndicator/></View>
      )
    }
    
    renderItems = ()=>{
      let data = this.state.data;
      return data.map((item,index)=>{
        return(
          <TouchableHighlight key = {index} style = {{elevation : 1,margin : 5,padding : 3, borderBottomWidth : 0, backgroundColor : this.state.theme=='white'? white.itemBackground :black.itemBackground, borderStyle : 'solid', }} onPress = {()=>{this.props.navigation.navigate("Video",{videoId : item.id.videoId, data : this.state.data, theme : this.state.theme})}} >
          <View>
            <ImageBackground source = {{uri : item.snippet.thumbnails.high.url}} style ={{height : 130}}>
            
            </ImageBackground>
            <View><Text style = {{ paddingVertical : 5, color : this.state.theme=='white'? white.textColor :black.textColor, fontSize:17}}>{item.snippet.title}</Text></View>
          </View>
          </TouchableHighlight>
        )
      })
    }
    renderItems1 = ()=>{
      let data = this.state.data;
      return data.map((item,index)=>{
        return(
          <TouchableHighlight underlayColor = {"rgba(180,180,180,0.5)"} key = {index} style = {{margin : 5,padding : 5,elevation : 1, borderBottomWidth : 0, backgroundColor : this.state.theme=='white'? white.itemBackground :black.itemBackground, borderStyle : 'solid'}}
           onPress = {()=>{this.props.navigation.navigate("Video",{videoId : item.id.videoId, data : this.state.data, theme : this.state.theme, token : this.state.token, apiKey : this.state.apiKey, channelId : this.state.channelId})}} >
            <View style = {{flexDirection : 'row'}}>
              <View style = {{flex : 0.4}}>
                <ImageBackground source = {{uri : item.snippet.thumbnails.medium.url}} style = {{height : 80}}></ImageBackground>
              </View>
              <View style = {{flex : 0.6, padding : 5}}>
                <Text style = {{color : this.state.theme=='white'? white.textColor :black.textColor}}>{item.snippet.title}</Text>
              </View>
            </View>
          </TouchableHighlight>
        )
      })
    }
  }
  const white = {
    mainBackgroundColor : 'rgba(180,180,180,0.2)',
    textColor : 'black',
    itemBackground : 'white'
  }
  const black = {
    mainBackgroundColor : 'rgb(10,10,10)',
    textColor : 'white',
    itemBackground : 'rgba(20,20,20,0.9)'
  }
  
