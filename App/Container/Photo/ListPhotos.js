import React, { Component } from "react";
import {
  Image,
  TouchableHighlight,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  Text,
  View
} from "react-native";
import PropTypes from "prop-types";
import { StackNavigator } from "react-navigation";
import { YouTubeStandaloneAndroid } from "react-native-youtube";
import YouTubeVideo from "./YouTubeVideo";
import Icon from "react-native-vector-icons/MaterialIcons";
import Icons from "react-native-vector-icons/MaterialCommunityIcons";

const apiKey = "AIzaSyDRk_jFZwhslzstmgovhpSEM-DVj_e2S94";
const channelId = "UCN-l2S52_LpVH8LfVmF9yeg";
const results = 50;

export default class ListPhotos extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: []
    };
  }
  static navigationOptions = {
    title: "YOUTUBE",
    headerTitleStyle: {
      flex: 1,
      textAlign: "center",
      color: "#fff"
    },    
    headerStyle: {
      backgroundColor: 'red'
   },  
 };

  componentDidMount() {
    console.log(this.props);
    fetch(
      `https://www.googleapis.com/youtube/v3/search/?key=${apiKey}&channelId=${channelId}&part=snippet,id&order=date&maxResults=${results}`
    )
      //fetch('https://www.googleapis.com/youtube/v3/search/?key=AIzaSyBJ3ntReiv0L19H2RoYW62LpRdIuyPhIpw&channelId=UCQzdMyuz0Lf4zo4uGcEujFw&part=snippet,id&order=date&maxResults=30')
      .then(res => res.json())
      .then(res => {
        console.log(res);
        const videoId = [];
        res.items.forEach(item => {
          videoId.push(item);
        });
        this.setState({
          data: videoId
        });
      })
      .catch(error => {
        console.error(error);
      });
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.body}>
            {this.state.data.map((item, i) => {
              return (
                <TouchableOpacity
                  key={item.id.videoId}
                  onPress={() => {
                    YouTubeStandaloneAndroid.playVideo({
                      apiKey: apiKey,
                      videoId: item.id.videoId,
                      autoplay: true
                    })
                      .then(() => console.log("Standalone Player Exited"))
                      .catch(errorMessage => console.error(errorMessage));
                  }}
                >
                  <View style={styles.vids}>
                    <Image
                      source={{ uri: item.snippet.thumbnails.medium.url }}
                      style={{ width: 400, height: 200 }}
                    />

                    <View style={styles.vidItems}>
                      <Image
                        source={require("./images/icon.png")}
                        style={{
                          width: 40,
                          height: 40,
                          borderRadius: 10, 
                          paddingRight:10
                          
                        }}
                      />
                      <Text style={styles.vidText}>{item.snippet.title}</Text>
                    </View>
                  </View>
                  {/* <Text>hi, hello</Text> */}
                </TouchableOpacity>
              );
            })}
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  separator: {
    flex: 1,
    height: StyleSheet.hairlineWidth,
    backgroundColor: "#8E8E8E"
  },
  body: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    padding: 30
  },
  vids: {
    paddingBottom: 30,
    width: 320,
    alignItems: "center",
    backgroundColor: "#fff",
    justifyContent: "center",
    borderColor: "#aaa"
  },

  vidItems: {
    flexDirection: "row",
    alignItems: "center",   
    borderBottomWidth: 1,
    borderBottomColor: "#D3D3D3"
  },
  vidText: {
    paddingLeft: 30,
    color: "#000",
    fontWeight:'bold',
    paddingVertical: 15
  },
  tabBar: {
    backgroundColor: "#fff",
    height: 70,
    flexDirection: "row",
    justifyContent: "space-around",
    borderTopWidth: 0.5,
    borderColor: "#bbb"
  },
  tabItems: {
    alignItems: "center",
    justifyContent: "center",
    paddingBottom: 5
  },
  tabTitle: {
    fontSize: 11,
    color: "#333",
    paddingTop: 4,
    textDecorationLine: "underline"
  }
});
