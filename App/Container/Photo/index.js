import { createStackNavigator } from "react-navigation";
import ListPhotos from './ListPhotos'
import YouTubeVideo from './YouTubeVideo'

const Photos = createStackNavigator(
  
  {
    ListPhotos: ListPhotos,
    YouTubeVideo: YouTubeVideo,
  },
  {
    initialRouteName: 'ListPhotos',
    header: null,
  }
)

export default Photos