import React, {Component} from 'react';
import {View} from 'react-native';
import {createAppContainer, createStackNavigator} from 'react-navigation';
import HomeScreen from './homescreen';
import VideoPlayer from './videoPlayer';

const Vid = createAppContainer(createStackNavigator({
    Home : {
        screen : HomeScreen,
        navigationOptions:()=>({
            header : null
        })
    },
    Video : {
        screen : VideoPlayer,
        navigationOptions:()=>({
            header : null
        })
    }
},
{
    initialRouteName : "Home"
}
))

//export default Vid;

export default class MainVideo extends Component{
    render(){
        return(
            <View style = {{backgroundColor : 'grey', flex:1}}>
                <Vid/>
            </View>
        )
    }
}
export default MainVideo;
