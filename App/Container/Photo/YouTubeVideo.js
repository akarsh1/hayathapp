import React from "react";
import { StyleSheet, View } from "react-native";
import { StackNavigator } from "react-navigation";
import YouTube, {YouTubeStandaloneAndroid} from "react-native-youtube";
 

export default class YouTubeVideo extends React.Component {
 

  static navigationOptions = {
    headerTitle: "YouTube",
    headerStyle: {
      backgroundColor: "red"
    },
    headerTitleStyle: {
      color: "red"
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <YouTubeStandaloneAndroid
          videoId={this.props.navigation.state.params.youtubeId}
          play={true}
          loop={false}
          rel={false}
          fullscreen={true}
          controls={1}
          apiKey={"AIzaSyCErV8p-9gdE5u29w-Nz7ALxvAWOmKyRjo"}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff"
  }
});
