/** @format */

import React, {
  StyleSheet,
  Platform,
  Dimensions,
  PixelRatio,
} from 'react-native'
import { Color, Constants } from '@common'

const { width, height, scale } = Dimensions.get('window'),
  vw = width / 100,
  vh = height / 100,
  vmin = Math.min(vw, vh),
  vmax = Math.max(vw, vh)

export default StyleSheet.create({
  body: {
    flex: 1,
  },
  flatlist: {
    flex: 1,
    width: width,
  },
  topBar: {
    width: width,
    height: 30,
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 15,
  },
  empty: {
    marginTop: 150,
    // marginRight: 70,
    // marginBottom: 30,
    marginLeft: 80,
    alignItems: 'center',
    justifyContent:'center',
    color: 'black', 
    fontSize:20,
    fontWeight:'bold'
  },
  emptyView: {
    alignItems: 'flex-start',
  },
})
