/** @format */

import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  scrollView: {
    marginTop: 10,
    paddingBottom: 10,
    backgroundColor: '#fff',
  },
  body: {
    flex: 1,
    backgroundColor: '#E02726',
  },
  navbar: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    alignItems: 'center',
    backgroundColor: '#fff',
    borderBottomColor: '#dedede',
    borderBottomWidth: 0,
    height: 20,
    justifyContent: 'center',
  },
})
