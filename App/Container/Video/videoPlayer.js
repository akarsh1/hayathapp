import React from "react";

import PropTypes from "prop-types";
 import Orientation from 'react-native-orientation';
import { View, StyleSheet, Dimensions, Text, Button } from "react-native";

import Video from "react-native-video";
const { height, width } = Dimensions.get('window')

class VideoPlayer extends React.Component {
  constructor(props) {
    super(props);

    this.state = { 
      paused: true 
    };
  }
  componentDidMount() {
     Orientation.lockToPortrait();
     
  } 

 
  componentDidUpdate(prevProps) {
    if (!this.props.isFocused && prevProps.isFocused) {
      this.setState({ paused: false }, () => this.setState({ paused: true }));
    }
  }
  
  render() {
    return (  
      <View style={styles.container}>
      
        <Video
          source={{
            uri: "rtmp://103.130.188.26:1935/ntv/live/live1"
          }} // Can be a URL or a local file.
          ref={ref => {
            this.player = ref;
          }} // Store reference
          controls={true}
          volume={1.0}
          muted={false}
          paused={this.state.paused} // Pauses playback entirely.
          resizeMode="stretch" // Fill the whole screen at aspect ratio.*
          repeat={true} // Repeat forever.
          playInBackground={false} // Audio continues to play when app entering background.
          progressUpdateInterval={250.0}
          style={{ height: height * .35, width: width * 1, backgroundColor:'black', }}
         />
 
        <Text>Video streaming....</Text>
 
       </View> 
    );
  }    
}

const styles = StyleSheet.create({
  container:{ flex: 1, justifyContent: "center"},
    backgroundVideo: {
      position: 'absolute',
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
    },
});

export default VideoPlayer;
