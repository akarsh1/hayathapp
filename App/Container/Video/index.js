/** @format */

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import ListVideo from "./ListVideo";
import VideoPlayer from "./videoPlayer";

export default class Videos extends PureComponent {
  static propTypes = {
    onViewPost: PropTypes.func,
    navigation: PropTypes.object,
    isFocused: PropTypes.bool
  };
  render() {
    const { onViewPost, navigation, isFocused } = this.props;
    return (
      <VideoPlayer
        onViewPost={onViewPost}
        navigation={navigation}
        isFocused={isFocused}
      />
    );
  }
}
