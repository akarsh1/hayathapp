import Constants from './Constants';
import Images from './Images'

export default {

  /**
   * There are so many choices from the marketplace mobile app, we thank you for choosing us and purchased the BeoNews products, please follow the step bellow to quickly set up your app
   * The detail document from: https://beonews.inspireui.com
   *
   * Step 1: Change your Wordpress site URL
   * Please make sure you have install all the Wordpress plugins - https://beonews.inspireui.com/step-2-installing-wordpress.html
   * On Mac OS, you need to create ~/Documents/FacebookSDK - https://beonews.inspireui.com/beonews-setup.html
  //  */
  // URL: {
  //   // root: 'http://news.inspireUI.com',
  //   root: 'http://mstore.local',
  // },

  /**
   * Step 2: config image and default layout post
   * HorizonLayout: show the horizontal layout config, show as the default homepage
   *    - Tags: the tag ids from Wordpress blog, it could be set as array [x, x...]
   *    - Categories: the category ids, , it could be set as array [x, x...]
   *    - row: the number of row support on the list, default is one row
   *    - paging: blog per page from swiping, default is false
   *    - layout: support 8 kind of layouts: banner, twoColumn, threeColumn, threeColumnHigh, list, listRight, card, flexColumn
   *      (flexColumn is flexible column layout that you can config both width & height)
   * Banner: option to show the banner home page, default visible is true
   * AdvanceLayout: mix layout config for AdvanceMode: card, column, list, banner, twoColumn, threeColumn, listRight
   */
  //
  HorizonLayout: [
    {tags: [18], paging: true, layout: Constants.Layout.banner},
    {name: "ರಾಜಕೀಯ", categories: [233], layout: Constants.Layout.twoColumn},
    {name: "ಅಂಕಣ", categories: [18], paging: true, row: 3, layout: Constants.Layout.list},
    {name: "ಆರೋಗ್ಯ ಸಂಜೀವಿನಿ", categories: [316], layout: Constants.Layout.column},
    {name: "ಕ್ರೀಡ", categories: [18], layout: Constants.Layout.flexColumn, width: 120, height: 250},
    {name: "ವಾಣಿಜ್ಯ", categories: [317], paging: true, row: 3, layout: Constants.Layout.listRight},
    {name: "ಅಡುಗೆ ಮನೆ", categories: [312], paging: true, layout: Constants.Layout.card},
  ],

 
  Banner: {
    visible: true,
    sticky: true,
    tag: [],
    categories: []
  },
  // The advance layout
  AdvanceLayout: [
    Constants.Layout.threeColumn,
    Constants.Layout.threeColumn,
    Constants.Layout.threeColumn,
    Constants.Layout.list,
    Constants.Layout.list,
    Constants.Layout.list,
    Constants.Layout.list,
    Constants.Layout.card,
    Constants.Layout.column,
    Constants.Layout.column,
  ],


  /**
   * Step 3: Advance config
   * CategoryVideo: config the category id for video page
   * imageCategories: config image for categories
   **/
  // Category video id from the menu
  CategoryVideo: 34,

  //Category images.........
  imageCategories: {
   
    "all": require("@images/category/cate4.jpg")
  },

  // Custom page from left menu side
  CustomPages: {
    contact_id: 11,
    aboutus_id: 7,
    politics_id:20
  },

  // config for Firebase, use to sync user data across device and favorite post
  Firebase: {
    apiKey: "AIzaSyAZhwel4Nd4T5dSmGB3fI_MUJj6BIz5Kk8",
    authDomain: "beonews-ef22f.firebaseapp.com",
    databaseURL: "https://beonews-ef22f.firebaseio.com",
    storageBucket: "beonews-ef22f.appspot.com",
    messagingSenderId: "1008301626030",
    readlaterTable: "list_readlater"
  },

  // config for log in by Facebook
  Facebook: {
    showAds: false,
    adPlacementID: "",
    logInID: "1809822172592320",
    sizeAds: "standard",// standard, large
  },


  // config for log in by Google
  Google: {
    analyticId: 'UA-90561349-1',
    androidClientId: '338838704385-1om86241pq2qpg4qi677jb1ndo5jqfh2.apps.googleusercontent.com',
    iosClientId: '338838704385-1om86241pq2qpg4qi677jb1ndo5jqfh2.apps.googleusercontent.com',
  },

  // config for log in by Admob
  AdMob: {
    visible: false,
    deviceID: "pub-2101182411274198",
    unitID: "ca-app-pub-2101182411274198/8802887662",
    unitInterstitial: "ca-app-pub-2101182411274198/7326078867",
    isShowInterstital: true
  },

  // tab animate
  tabBarAnimate: Constants.Animate.zoomIn,

  // config default for left menu
  LeftMenuStyle: Constants.LeftMenu.scale
}
