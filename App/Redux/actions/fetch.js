
import { Constants } from '@common';

const fetch = (dispatch, api, type, extra = {}) => api.get((err, data) => {
  // check the paging is finish
  let finish = false;
  console.log(data)

  if (err) {
    console.log(err);
  }

  if (typeof data == 'undefined') {
    finish = true;
    dispatch({ type: type, payload: [], extra, finish });
  }

  // if (typeof data == 'undefined' || typeof data._paging.next == 'undefined')  {
  //     // finish = true;
  // }
  console.log("wat is the data'", data)

  dispatch({ type: type, payload: data, extra, finish });
});

export default fetch;
